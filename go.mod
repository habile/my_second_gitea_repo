module gitea.com/habile/my_second_gitea_repo

go 1.15

require code.gitea.io/sdk/gitea v0.15.0

require github.com/hashicorp/go-version v1.2.1 // indirect
