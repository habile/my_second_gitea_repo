ARG GOVERSION="1.16.2"

FROM golang:${GOVERSION}-alpine AS buildenv

ARG GOOS="linux"

COPY . $GOPATH/src/
WORKDIR $GOPATH/src

RUN	apk add --quiet --no-cache \
		build-base \
		make \
		git && \
	make clean build STATIC=true

FROM scratch
ARG VERSION="0.7.0"
COPY --from=buildenv /go/src/my_second_gitea_repo /
ENV HOME="/app"
ENTRYPOINT ["/my_second_gitea_repo"]
